// Lab3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstdlib>
#include <limits>
#include "windows.h"
#include <time.h>
#include <omp.h>
#include <float.h>
#include "intrin.h"

using namespace std;

#define _M_X64

typedef enum{
	SSE,
	SSE2,
	SSE3,
	SSSE3,
	SSE41,
	SSE42,
	AVX,
	AVX2
}SIMD;

typedef struct{
	float re;
	float im;
} Complex, *PComplex;

bool AVXSupport()//������ ��� 13 ������ � ����
{
	// OS support check
	__int64 xcr0 = _xgetbv(0);
	bool result = (xcr0 & 7) == 7;
	if (!result)
	{
		return false;
	}

	int regs[4];
	__cpuid(regs, 1);
	// ecx 7 -> 111
	result = ((regs[2] >> 26) & 7) == 7;

	return result;
}

unsigned MaxFun(unsigned &MaxExt)
{
	int regs[4];
	__cpuid(regs, 0);
	unsigned res = regs[0];
	__cpuid(regs, 0x80000000);
	MaxExt = regs[0];
	return res;
}

unsigned SIMDSupported()
{
	unsigned maxFunc, maxExt;
	maxFunc = MaxFun(maxExt);
	/*if (maxFunc << 5 || maxExt < 0x800000001)
	{
	return 0;
	}*/

	bool bAVX;
	bAVX = AVXSupport();
	if (bAVX)
	{
		return (1 << (AVX + 1)) - 1;
	}

	int regs[4];
	__cpuid(regs, 1);

	bool bSSE = regs[2] & (1 << 20) == (1 << 20);
	if (bSSE)
	{
		return (1 << (SSE42 + 1)) - 1;
	}
	bSSE = regs[2] & (1 << 19) == (1 << 19);
	if (bSSE)
	{
		return (1 << (SSE41 + 1)) - 1;
	}
	bSSE = regs[2] & (1 << 9) == (1 << 9);
	if (bSSE)
	{
		return (1 << (SSSE3 + 1)) - 1;
	}
	bSSE = regs[2] & (1 << 0) == (1 << 0);
	if (bSSE)
	{
		return (1 << (SSE3 + 1)) - 1;
	}
	bSSE = regs[3] & (1 << 26) == (1 << 26);
	if (bSSE)
	{
		return (1 << (SSE2 + 1)) - 1;
	}
	bSSE = regs[3] & (1 << 25) == (1 << 25);
	if (bSSE)
	{
		return (1 << (SSE + 1)) - 1;
	}

	return 0;
}

void printSeparator()
{
	printf("------------------------------------\n");
}

void print8(INT8* array, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%i ", array[i]);
	}
	printf("\n");
}

void print64(INT64* array, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%i ", array[i]);
	}
	printf("\n");
}

void printFloat(float* array, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%f ", array[i]);
	}
	printf("\n");
}

// SUM_ABS
INT8* sumAbs8(INT8* x, INT8* y, int n)
{
	INT8 *xAbs = (INT8*)malloc(n * sizeof(INT8));
	INT8 *yAbs = (INT8*)malloc(n * sizeof(INT8));
	INT8 *z = (INT8*)malloc(n * sizeof(INT8));

	for (int i = 0; i < n; i++)
	{
		xAbs[i] = abs(x[i]);
		yAbs[i] = abs(y[i]);
		z[i] = xAbs[i] + yAbs[i];
	}

	free(xAbs);
	free(yAbs);

	return z;
}

INT16* sumAbs16(INT16* x, INT16* y, int n)
{
	INT16 *xAbs = (INT16*)malloc(n * sizeof(INT16));
	INT16 *yAbs = (INT16*)malloc(n * sizeof(INT16));
	INT16 *z = (INT16*)malloc(n * sizeof(INT16));

	for (int i = 0; i < n; i++)
	{
		xAbs[i] = abs(x[i]);
		yAbs[i] = abs(y[i]);
		z[i] = xAbs[i] + yAbs[i];
	}

	free(xAbs);
	free(yAbs);

	return z;
}

INT32* sumAbs32(INT32* x, INT32* y, int n)
{
	INT32 *xAbs = (INT32*)malloc(n * sizeof(INT32));
	INT32 *yAbs = (INT32*)malloc(n * sizeof(INT32));
	INT32 *z = (INT32*)malloc(n * sizeof(INT32));

	for (int i = 0; i < n; i++)
	{
		xAbs[i] = abs(x[i]);
		yAbs[i] = abs(y[i]);
		z[i] = xAbs[i] + yAbs[i];
	}

	free(xAbs);
	free(yAbs);

	return z;
}

INT64* sumAbs64(INT64* x, INT64* y, int n)
{
	INT64 *xAbs = (INT64*)malloc(n * sizeof(INT64));
	INT64 *yAbs = (INT64*)malloc(n * sizeof(INT64));
	INT64 *z = (INT64*)malloc(n * sizeof(INT64));

	for (int i = 0; i < n; i++)
	{
		xAbs[i] = abs(x[i]);
		yAbs[i] = abs(y[i]);
		z[i] = xAbs[i] + yAbs[i];
	}

	free(xAbs);
	free(yAbs);

	return z;
}

float* sumAbsFloat(float* x, float* y, int n)
{
	float *xAbs = (float*)malloc(n * sizeof(float));
	float *yAbs = (float*)malloc(n * sizeof(float));
	float *z = (float*)malloc(n * sizeof(float));

	for (int i = 0; i < n; i++)
	{
		xAbs[i] = abs(x[i]);
		yAbs[i] = abs(y[i]);
		z[i] = xAbs[i] + yAbs[i];
	}

	free(xAbs);
	free(yAbs);

	return z;
}

double* sumAbsDouble(double* x, double* y, int n)
{
	double *xAbs = (double*)malloc(n * sizeof(double));
	double *yAbs = (double*)malloc(n * sizeof(double));
	double *z = (double*)malloc(n * sizeof(double));

	for (int i = 0; i < n; i++)
	{
		xAbs[i] = abs(x[i]);
		yAbs[i] = abs(y[i]);
		z[i] = xAbs[i] + yAbs[i];
	}

	free(xAbs);
	free(yAbs);

	return z;
}
// SQRT
float* sqrtFloat(float* x, int n)
{
	float *result = (float*)malloc(n * sizeof(float));

	for (int i = 0; i < n; i++)
	{
		result[i] = sqrt(x[i]);
	}
	return result;
}

double* sqrtDouble(double* x, int n)
{
	double *result = (double*)malloc(n * sizeof(double));

	for (int i = 0; i < n; i++)
	{
		result[i] = sqrt(x[i]);
	}
	return result;
}

void cmul(PComplex a, PComplex b, PComplex c, int n)
{
	//(a+bi)*(c+di) = (ac-bd) + i(bc+da);
	for (int i = 0; i < n; i++)
	{
		c[i].re = a[i].re * b[i].re - a[i].im * b[i].im;
		c[i].im = a[i].re * b[i].im + a[i].im * b[i].re;
	}
}

//void oneBitShift(unsigned* array)
//{
//	unsigned mask = 1 << 31;
//	unsigned lost = 0;
//	for(int i = 14; i > 0; i--){
//		lost = (mask & array[i]) >> 31;
//		array[i] = array[i] << 1;
//		array[i] &= lost;
//	}
//}

// SSE
INT8* SSESumAbs8(INT8* x, INT8* y, int n)
{
	__m128i *px = (__m128i*)x;
	__m128i *py = (__m128i*)y;
	INT8 *z = (INT8*)_aligned_malloc(n * sizeof(INT8), sizeof(INT8) * 8);
	__m128i *pz = (__m128i*)z;

	for (int i = 0; i < n / 16; i++)
	{
		px[i] = _mm_abs_epi8(px[i]);
		py[i] = _mm_abs_epi8(py[i]);
		pz[i] = _mm_add_epi8(px[i], py[i]);
	}

	return z;
}

INT16* SSESumAbs16(INT16* x, INT16* y, int n)
{
	__m128i *px = (__m128i*)x;
	__m128i *py = (__m128i*)y;
	INT16 *z = (INT16*)_aligned_malloc(n * sizeof(INT16), sizeof(INT16));
	__m128i *pz = (__m128i*)z;

	for (int i = 0; i < n / 8; i++)
	{
		px[i] = _mm_abs_epi16(px[i]);
		py[i] = _mm_abs_epi16(py[i]);
		pz[i] = _mm_add_epi16(px[i], py[i]);
	}

	return z;
}

INT32* SSESumAbs32(INT32* x, INT32* y, int n)
{
	__m128i *px = (__m128i*)x;
	__m128i *py = (__m128i*)y;
	INT32 *z = (INT32*)_aligned_malloc(n * sizeof(INT32), sizeof(INT32));
	__m128i *pz = (__m128i*)z;

	for (int i = 0; i < n / 4; i++)
	{
		px[i] = _mm_abs_epi32(px[i]);
		py[i] = _mm_abs_epi32(py[i]);
		pz[i] = _mm_add_epi32(px[i], py[i]);
	}

	return z;
}

INT64* SSESumAbs64(INT64* x, INT64* y, int n)
{
	for (int i = 0; i < n; i++)
	{
		x[i] = abs(x[i]);
		y[i] = abs(y[i]);
	}

	__m128i *px = (__m128i*)x;
	__m128i *py = (__m128i*)y;
	INT64 *z = (INT64*)_aligned_malloc(n * sizeof(INT64), sizeof(INT64) * 8);
	__m128i *pz = (__m128i*)z;

	for (int i = 0; i < n / 2; i++)
	{
		pz[i] = _mm_add_epi64(px[i], py[i]);
	}

	return z;
}

__m128 floatAbs(__m128 value)
{
	__m128 reverted = _mm_sub_ps(_mm_setzero_ps(), value);
	return _mm_max_ps(value, reverted);
}

__m128d doubleAbs(__m128d value)
{
	__m128d reverted = _mm_sub_pd(_mm_setzero_pd(), value);
	return _mm_max_pd(value, reverted);
}

float* SSESumAbsFloat(float* x, float* y, int n)
{
	__m128 *px = (__m128*)x;
	__m128 *py = (__m128*)y;
	float *z = (float*)_aligned_malloc(n * sizeof(float), sizeof(float) * 8);
	__m128 *pz = (__m128*)z;

	for (int i = 0; i < n / 4; i++)
	{
		pz[i] = _mm_add_ps(floatAbs(px[i]), floatAbs(py[i]));
	}

	return z;
}

double* SSESumAbsDouble(double* x, double* y, int n)
{
	__m128d *px = (__m128d*)x;
	__m128d *py = (__m128d*)y;
	double *z = (double*)_aligned_malloc(n * sizeof(double), sizeof(double) * 8);
	__m128d *pz = (__m128d*)z;

	for (int i = 0; i < n / 2; i++)
	{
		pz[i] = _mm_add_pd(doubleAbs(px[i]), doubleAbs(py[i]));
	}

	return z;
}
// SQRT
float* SSESqrtFloat(float* x, int n)
{
	float* res = (float*)_aligned_malloc(n * sizeof(float), sizeof(float) * 8);
	__m128 *px = (__m128*)x;
	__m128 *pRes = (__m128*)res;

	for (int i = 0; i < n / 4; i++)
	{
		pRes[i] = _mm_sqrt_ps(px[i]);
	}
	return res;
}

double* SSESqrtDlouble(double* x, int n)
{
	double* res = (double*)_aligned_malloc(n * sizeof(double), sizeof(double) * 8);
	__m128d *px = (__m128d*)x;
	__m128d *pRes = (__m128d*)res;

	for (int i = 0; i < n / 2; i++)
	{
		pRes[i] = _mm_sqrt_pd(px[i]);
	}
	return res;
}

void SSECmul(PComplex a, PComplex b, PComplex c, int n)
{
	__m128 *pa = (__m128*)a;
	__m128 *pb = (__m128*)b;
	__m128 *pc = (__m128*)c;

	//(a+bi)*(c+di) = (ac-bd) [+] i(bc+da);
	for (int i = 0; i < n / 2; i++)
	{
		__m128 temp1 = _mm_shuffle_ps(pa[i], pa[i], _MM_SHUFFLE(2, 2, 0, 0));
		temp1 = _mm_mul_ps(temp1, pb[i]);
		__m128 temp2 = _mm_shuffle_ps(pa[i], pa[i], _MM_SHUFFLE(3, 3, 1, 1));
		__m128 temp3 = _mm_shuffle_ps(pb[i], pb[i], _MM_SHUFFLE(2, 3, 0, 1));
		temp2 = _mm_mul_ps(temp2, temp3);
		pc[i] = _mm_addsub_ps(temp1, temp2);
	}

	// a[0].re   a[0].re  a[1].re  a[1].re
	//*
	// b[0].re   b[0].im  b[1].re  b[1].im
	//------------------------------------
	//(1)
	//a[o].im  a[0].im  a[1].im  a[1].im
	//*
	//b[0].im  b[0].re  b[1].im  b[1].re
	//--------------------------------------
	//a[0].im * b[0].im   im*re  im*im  im*re

	//addsub
}

// SHIFT
unsigned* generateRandonUnsigneds(int n)
{
	unsigned* res = (unsigned*)_aligned_malloc(32 * n, 32);
	for (int i = 0; i < n; i++)
	{
		res[i] = rand() * rand() * rand();
	}
	return res;
}

void shift(unsigned* a, unsigned* res)
{
	for (int i = 15; i >= 1; i--)
	{
		res[i] = (a[i] << 1) | (a[i - 1] >> 31);
	}
	res[0] = a[0] << 1;
}

void SSEShift(unsigned* a, unsigned* res)
{
	__m128i* mA = (__m128i*) a;
	__m128i* mRes = (__m128i*) res;
	__m128i tmp = _mm_set1_epi32(0x80000000);
	__m128i dop[4];

	dop[0] = _mm_cvtsi32_si128(a[3] >> 31);
	dop[1] = _mm_cvtsi32_si128(a[7] >> 31);
	dop[2] = _mm_cvtsi32_si128(a[11] >> 31);

	mRes[0] = _mm_or_si128(_mm_slli_epi32(mA[0], 1), _mm_srli_epi32(_mm_slli_si128(_mm_and_si128(tmp, mA[0]), 4), 31));

	mRes[1] = _mm_or_si128(_mm_or_si128(_mm_slli_epi32(mA[1], 1), _mm_srli_epi32(_mm_slli_si128(
		_mm_and_si128(tmp, mA[1]), 4), 31)), dop[0]);

	mRes[2] = _mm_or_si128(_mm_or_si128(_mm_slli_epi32(mA[2], 1), _mm_srli_epi32(_mm_slli_si128(
		_mm_and_si128(tmp, mA[2]), 4), 31)), dop[1]);

	mRes[3] = _mm_or_si128(_mm_or_si128(_mm_slli_epi32(mA[3], 1), _mm_srli_epi32(_mm_slli_si128(
		_mm_and_si128(tmp, mA[3]), 4), 31)), dop[2]);
}

void print512Bits(unsigned *a)
{
	for (int i = 15; i >= 0; i--)
	{
		for (int j = 31; j >= 0; j--)
		{
			if ((a[i] & (1 << j)) > 0)
			{
				printf("1");
			}
			else
			{
				printf("0");
			}
		}
		printf("|\n");
	}
}

boolean testEqualityShift()
{
	//unsigned* a = (unsigned*)_aligned_malloc(512, 32);
	unsigned* a = generateRandonUnsigneds(16);
	print512Bits(a);
	printf("\n\n");
	unsigned* res = (unsigned*)_aligned_malloc(512, 32);
	unsigned* res2 = (unsigned*)_aligned_malloc(512, 32);

	shift(a, res);
	print512Bits(res);
	printf("\n\n");
	SSEShift(a, res2);
	print512Bits(res2);
	printf("\n\n");

	for (int i = 0; i < 16; i++)
	{
		if (res[i] != res2[i])
			return false;
	}
	return true;
}

bool equals(float value1, float value2)
{
	return ((((value1 - value2) / value1) < 0.1) && (((value1 - value2) / value1) > -0.1));
}

bool equals(PComplex first, PComplex second, int n)
{
	for (int i = 0; i < n; i++)
	{
		if (!equals(first[i].re, second[i].re) || !equals(first[i].im, second[i].im)){
			return true;
		}
	}
	return false;
}

int _tmain(int argc, _TCHAR* argv[])
{
	//int N = 16777216;
	int N = 4096*4;
	//int N = 16;
	int TESTS_NUMBER = 10;
	srand(time(0));

	__int64 frequency;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);
	printf("frequency.Low==>%i\n", frequency);
	printSeparator();
	__int64 start = 0, finish = 0;
	__int64 diffSumAbs32 = MAXINT64;
	__int64 diffSumAbs32SSE = MAXINT64;
	__int64 diffSumAbs64 = MAXINT64;
	__int64 diffSumAbs64SSE = MAXINT64;

	__int64 diffSimpleSQRT = MAXINT64;
	__int64 diffSSESQRT = MAXINT64;

	__int64 diffComplex = MAXINT64;
	__int64 diffSSEComplex = MAXINT64;

	__int64 diffShift = MAXINT64;
	__int64 diffSSEShift = MAXINT64;


	//// SUM_ABS_32
	//INT32 *int32ar = (INT32*)_aligned_malloc(N * sizeof(INT32), sizeof(INT32)* 8);
	//INT32 *int32ar2 = (INT32*)_aligned_malloc(N * sizeof(INT32), sizeof(INT32)* 8);
	//for (int i = 0; i < N; i++)
	//{
	//	int32ar[i] = rand() % 1000;
	//	int32ar2[i] = rand() % 1000;
	//}
	//// Simple SUM_ABS_32
	//for (int t = 0; t < TESTS_NUMBER; t++) {
	//	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	//	INT32* int32res = sumAbs32(int32ar, int32ar2, N);
	//	QueryPerformanceCounter((LARGE_INTEGER *)&finish);

	//	if (finish - start < diffSumAbs32) {
	//		diffSumAbs32  = finish - start;
	//	}

	//	//Free memory
	//	free(int32res);
	//}
	//printf("Simple sum abs 32 time==>%f ms\n", diffSumAbs32 * 1000.0 / frequency);
	//// SSE SUM_ABS_32
	//for (int t = 0; t < TESTS_NUMBER; t++) {
	//	QueryPerformanceCounter((LARGE_INTEGER *)&start);

	//	INT32* int32res = SSESumAbs32(int32ar, int32ar2, N);
	//	QueryPerformanceCounter((LARGE_INTEGER *)&finish);

	//	if (finish - start < diffSumAbs32SSE) {
	//		diffSumAbs32SSE  = finish - start;
	//	}

	//	//Free memory
	//	_aligned_free(int32res);
	//}
	//printf("SSE sum abs 32 time==>%f ms\n", diffSumAbs32SSE * 1000.0 / frequency);





	//

	//// SUM_ABS_64
	//INT64 *int64ar = (INT64*)_aligned_malloc(N * sizeof(INT64), sizeof(INT64)* 8);
	//INT64 *int64ar2 = (INT64*)_aligned_malloc(N * sizeof(INT64), sizeof(INT64)* 8);
	//for (int i = 0; i < N; i++)
	//{
	//	int64ar[i] = rand() % 1000;
	//}
	//// Simple SUM_ABS_32
	//for (int t = 0; t < TESTS_NUMBER; t++) {
	//	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	//	INT64* int64res = sumAbs64(int64ar, int64ar2, N);
	//	QueryPerformanceCounter((LARGE_INTEGER *)&finish);

	//	if (finish - start < diffSumAbs64) {
	//		diffSumAbs64 = finish - start;
	//	}

	//	//Free memory
	//	free(int64res);
	//}
	//printf("Simple sum abs 64 time==>%f ms\n", diffSumAbs64 * 1000.0 / frequency);
	//// SSE SUM_ABS_64
	//for (int t = 0; t < TESTS_NUMBER; t++) {
	//	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	//	INT64* int64res = SSESumAbs64(int64ar, int64ar2, N);
	//	QueryPerformanceCounter((LARGE_INTEGER *)&finish);

	//	if (finish - start < diffSumAbs64SSE) {
	//		diffSumAbs64SSE = finish - start;
	//	}

	//	//Free memory
	//	_aligned_free(int64res);
	//}
	//printf("SSE sum abs 64 time==>%f ms\n", diffSumAbs64SSE * 1000.0 / frequency);





	//// COMPLEX
	//PComplex complex = (PComplex)_aligned_malloc(N * sizeof(Complex), sizeof(Complex)* 8);
	//PComplex complex2 = (PComplex)_aligned_malloc(N * sizeof(Complex), sizeof(Complex)* 8);
	//PComplex simpleComplex = (PComplex)_aligned_malloc(N * sizeof(Complex), sizeof(Complex)* 8);
	//PComplex SSEComplex = (PComplex)_aligned_malloc(N * sizeof(Complex), sizeof(Complex)* 8);
	//for (int i = 0; i < N; i++)
	//{
	//	complex[i].re = (rand() % 1000) / 10.0;
	//	complex[i].im = (rand() % 1000) / 10.0;
	//}
	//// Simple Complex
	//for (int t = 0; t < TESTS_NUMBER; t++) {
	//	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	//	cmul(complex, complex2, simpleComplex, N);
	//	QueryPerformanceCounter((LARGE_INTEGER *)&finish);

	//	if (finish - start < diffComplex) {
	//		diffComplex = finish - start;
	//	}
	//}
	//printf("Simple complex time==>%f ms\n", diffComplex * 1000.0 / frequency);
	//// SSE Complex
	//for (int t = 0; t < TESTS_NUMBER; t++) {
	//	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	//	SSECmul(complex, complex2, SSEComplex, N);
	//	QueryPerformanceCounter((LARGE_INTEGER *)&finish);

	//	if (finish - start < diffSSEComplex) {
	//		diffSSEComplex = finish - start;
	//	}
	//}
	//printf("SSE complex time==>%f ms\n", diffSSEComplex * 1000.0 / frequency);

	//if (equals(simpleComplex, SSEComplex, N)){
	//	printf("+");
	//}
	//else
	//{
	//	printf("-");
	//}


	
	//// SQRT
	//float* x = (float*)_aligned_malloc(N * sizeof(float), sizeof(float)* 8);
	//for (int i = 0; i < N; i++)
	//{
	//	x[i] = ((rand() % 1000)) / 10.0;
	//}
	////Simple SQRT
	//for (int t = 0; t < TESTS_NUMBER; t++) {
	//	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	//	float* sqrt = sqrtFloat(x, N);
	//	QueryPerformanceCounter((LARGE_INTEGER *)&finish);

	//	if (finish - start < diffSimpleSQRT) {
	//		diffSimpleSQRT = finish - start;
	//	}
	//
	//	//Free memory
	//	free(sqrt);
	//}
	//printf("Simple SQRT time==>%f ms\n", diffSimpleSQRT * 1000.0 / frequency);
	//
	//// SSE SQRT
	//for (int t = 0; t < TESTS_NUMBER; t++) {
	//	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	//	float* SSESqrt = SSESqrtFloat(x, N);
	//	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	//
	//	if (finish - start < diffSSESQRT) {
	//		diffSSESQRT = finish - start;
	//	}
	//
	//	//Free memory
	//	_aligned_free(SSESqrt);
	//}
	//printf("SSE SQRT time==>%f ms\n", diffSSESQRT * 1000.0 / frequency);



	if (testEqualityShift())
	{
		printf("Shifts are equal");
	}
	else
	{
		printf("Shifts are DIFFERENT!!!");
	}
	unsigned *a = generateRandonUnsigneds(16);
	int cycles = 1000000;
	unsigned** results = (unsigned**)_aligned_malloc(cycles * sizeof(unsigned*), sizeof(unsigned*));
	unsigned* result = (unsigned*) malloc(16 * sizeof(unsigned));
	for (int t = 0; t < TESTS_NUMBER; t++) {
		QueryPerformanceCounter((LARGE_INTEGER *)&start);
		for (int i = 0; i < cycles; i++)
		{
			shift(a, result);
		}
		QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	
		if (finish - start < diffShift) {
			diffShift = finish - start;
		}

		/*for (int i = 0; i < cycles; i++)
		{
			free(results[i]);
		}*/
	}
	printf("Shift time==>%f ms\n", diffShift * 1000.0 / frequency);


	for (int t = 0; t < TESTS_NUMBER; t++) {
		QueryPerformanceCounter((LARGE_INTEGER *)&start);
		for (int i = 0; i < cycles; i++)
		{
			SSEShift(a, result);
		}
		QueryPerformanceCounter((LARGE_INTEGER *)&finish);

		if (finish - start < diffSSEShift) {
			diffSSEShift = finish - start;
		}

		/*for (int i = 0; i < cycles; i++)
		{
			_aligned_free(results[i]);
		}*/
	}
	printf("Shift SSE time==>%f ms\n", diffSSEShift * 1000.0 / frequency);

	getchar();
	return 0;
}

